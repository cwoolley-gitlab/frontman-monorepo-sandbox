# Frontman Monorepo Sandbox

Experimenting with using Frontman in a monorepo structure:

```
/shared/data
/shared/helpers
/shared/source
/shared/views/layouts
/shared/views/partials
/sites/site-one/config.rb
/sites/site-one/data
/sites/site-one/helpers
/sites/site-one/source
/sites/site-one/views/layouts
/sites/site-one/views/partials
```

Requirements:

* `frontman build` can be run from sub-site `/sites/site-one`, and will use the `config.rb` in that directory to build all files under `/sites/site-one/source`.
* Files under `/sites/site-one/source` can reference data/helpers/layouts/partials from directories under both `/shared/...` and `/sites/site-one/...`.
* Sub sites can generate built files into top-level `build` directory 
