# frozen_string_literal: true

# This has to be first in the config, if it is later, it will not be set when Frontman::App#register_layout is called
Frontman::Config.set(:layout_dir, '/' + Dir.pwd + '/../../views/layouts') # TODO: This has to be an absolute path, Frontman::App#register_layout doesn't make it relative
Frontman::Config.set(:partial_dir, '/../../views/partials')

# TODO: Cannot give a relative paths here, because register_data_dirs wants to turn it into a method which will be called with `define_singleton_method`
# The expectation would be that all of the files under these directories would be exposed under `data` to the templates, but that's not the case
# Had to fall back to symlinking, which is not desirable.
# register_data_dirs(['/../../data'])
register_data_dirs(['data'])
register_helper_dir('/../../helpers')

register_layout 'sitemap.xml', nil
register_layout '*', 'main.erb'
# Or use 'main.haml' if you prefer using HAML:
# register_layout '*', 'main.haml'

Frontman::Config.set(:domain, 'https://site-one.example.com')
Frontman::Config.set(:public_dir, '../../public')
# Frontman::Config.set(:build_directory, '../../build') # Currently not settable

Frontman::Bootstrapper.resources_from_dir(
  'source/'
).each do |resource|
  sitemap_tree.add(resource)
end

# TODO: This does not work, because RenderHelper#partial is hardcoded to look only in a single configured
# `partial_dir`.  It does not have the concept of multiple partial directories
# Add in top-level dirs
# %w[
#   ../../views/layouts
#   ../../views/partials
# ].each do |relative_dir_to_add|
#   Frontman::Bootstrapper.resources_from_dir(
#     # TODO: shouldn't require leading slash, it is getting incorrectly stripped somewhere
#     '/' + Dir.pwd + '/' + relative_dir_to_add
#   ).each do |resource|
#     sitemap_tree.add(resource)
#   end
# end
